//
//  HoroscopeDetailCell.swift
//  Tarot
//
//  Created by vu trong thien on 9/21/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit

class HoroscopeDetailCell: UICollectionViewCell {

    @IBOutlet weak var horoscopeImage: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()
        horoscopeImage.image = nil
    }

    func bindData(name: String) {
        horoscopeImage.contentMode = .scaleAspectFill
        horoscopeImage.image = UIImage(named: "\(name.lowercased())9")
    }

}
