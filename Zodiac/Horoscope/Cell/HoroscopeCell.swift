//
//  HoroscopeCollectionViewCell.swift
//  Tarot
//
//  Created by vu trong thien on 9/21/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit

class HoroscopeCell: UICollectionViewCell {

    @IBOutlet weak var horoscopeImage: UIImageView!
    @IBOutlet weak var horosopeName: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        horosopeName.text = nil
        horoscopeImage.image = nil
    }

    func bindData(name: String) {
        horoscopeImage.image = UIImage(named: "\(name.lowercased())9")
        horosopeName.text = name
    }

}
