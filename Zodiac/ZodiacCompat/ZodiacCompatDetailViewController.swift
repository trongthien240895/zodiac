//
//  ZodiacCompatDetailViewController.swift
//  Tarot
//
//  Created by VuTrongThien on 9/26/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit

class ZodiacCompatDetailViewController: UIViewController {

    @IBOutlet weak var compatTextView: UITextView!
    @IBOutlet weak var zodiacParnerImage: UIImageView!
    @IBOutlet weak var zodiacImage: UIImageView!
    @IBOutlet weak var zodiacParnerView: UIView!
    @IBOutlet weak var zodiacView: UIView!
    
    var names = ["", ""]
    var compatDetail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpNavigationBar()
    }

    func setUpView() {
        zodiacView.setOvalView()
        zodiacParnerView.setOvalView()
        zodiacImage.image = UIImage(named: "\(names[0].lowercased())1")
        zodiacParnerImage.image = UIImage(named: "\(names[1].lowercased())1")
        compatTextView.text = compatDetail
    }

    func setUpNavigationBar() {
        navigationController?.clearColor()
        navigationItem.addImageNavigation(name: "logo-zodiac", color: .white)
    }
    
}
