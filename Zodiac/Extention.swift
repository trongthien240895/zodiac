//
//  Extention.swift
//  Tarot
//
//  Created by vu trong thien on 9/6/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    func rotateImage() {
        UIView.animate(withDuration: 1, animations: {
            self.transform = CGAffineTransform(rotationAngle: 180)
        }) { (finish: Bool) in
            UIView.animate(withDuration: 0.5, animations: {
                self.transform = CGAffineTransform.identity
            })
        }
    }

    func scaleImage(scale: CGFloat = 0.5) {
        UIView.animate(withDuration: 0.5, animations: {
            self.transform = CGAffineTransform(scaleX: scale, y: scale)
        }) { (finish: Bool) in
            UIView.animate(withDuration: 0.5, animations: {
                self.transform = CGAffineTransform.identity
            })
        }
    }

}

extension UINavigationController {

    func clearColor() {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
    }

}

extension UINavigationItem {

    func addImageNavigation(name: String, color: UIColor) {
        let imageView = UIImageView()
        imageView.image = UIImage(named: name)
        imageView.contentMode = .scaleAspectFill
        imageView.tintColor = color
        imageView.frame = CGRect(x: 0, y: 0, width: 45, height: 30)
        titleView = imageView
    }

}

extension UIView {

    func setOvalView() {
        if frame.width >= frame.height {
            layer.cornerRadius = CGFloat(frame.width)/2.0
        } else{
            layer.cornerRadius = CGFloat(frame.height)/2.0
        }
    }

}

extension UITabBar {

    func clearColor()  {
        for view in subviews {
            view.backgroundColor = .clear
        }
    }
}
