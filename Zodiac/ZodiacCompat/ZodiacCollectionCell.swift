//
//  ZodiacCell.swift
//  Tarot
//
//  Created by VuTrongThien on 9/26/18.
//  Copyright © 2018 VuTrongThien. All rights reserved.
//

import UIKit

class ZodiacCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var zodiacImage: UIImageView!
    @IBOutlet weak var zodiacName: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        zodiacName.text = nil
        zodiacImage.image = nil
    }
    
    func bindData(name: String) {
        zodiacName.text = name
        zodiacName.textColor = .white
        zodiacImage.image = UIImage(named: "\(name.lowercased())1")
    }
    
}
